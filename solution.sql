CREATE TABLE IF NOT EXISTS CONTENTS (
    name TEXT,
    type TEXT
);




insert into CONTENTS
    values('WHITE_TILE','TILE'),
            ('BLACK_TILE','TILE'),
            ('RED_PIECE','PIECE'),
            ('BLACK_PIECE','PIECE');




CREATE TABLE IF NOT EXISTS  dic (
    key TEXT,
    value INT
);




insert into dic
    values('counter', 0);


create or replace function check_validity(SrcRow INT, SrcCol INT, DstRow INT, DstCol INT) returns boolean as $$
DECLARE
src_tile_content TEXT;
dst_tile_content TEXT;
begin
/*if source is tile or destination is tile then throw*/
SELECT TILE_CONTENT into src_tile_content from BOARD WHERE row=SrcRow and col=SrcCol;
SELECT TILE_CONTENT into dst_tile_content from BOARD WHERE row=DstRow and col=DstCol;


IF (src_tile_content in (select name from CONTENTS where type='TILE')) then
    return 'false';
    end if;
IF (dst_tile_content in (select name from CONTENTS where type='PIECE')) then
    return 'false';
    end if;




IF (src_tile_content='BLACK_PIECE') then
    IF ((SrcRow-DstRow!=1) OR ((select mod(value,2) from dic where key='counter')=0)) then
    return 'false';
    end if;
end if;


IF (src_tile_content='RED_PIECE') then
    IF ((SrcRow-DstRow!=(-1)) OR ((select mod(value,2) from dic where key='counter')=1)) then
     return 'false';
     end if;
end if;


IF (ABS(SrcCol-DstCol)!=1) then
return 'false';
end if;


return 'true';
end;
$$language plpgsql;


create or replace function make_move() returns trigger as $$
begin


if not check_validity(new.SrcRow, new.SrcCol, new.DstRow, new.DstCol) then
return null;
end if;


/*if (SELECT TILE_CONTENT from BOARD WHERE row=new.SrcRow and col=new.SrcCol) in (select name from CONTENTS where type='PIECE') and
    ((SELECT TILE_CONTENT from BOARD WHERE row=new.DstRow and col=new.DstCol) in (select name from CONTENTS where type='TILE'))
 then
    begin*/


    UPDATE dic
        SET value=(select value+1 from dic where key='counter')
        WHERE key='counter';


    UPDATE BOARD
        SET TILE_CONTENT=(SELECT TILE_CONTENT from BOARD WHERE row=new.SrcRow and col=new.SrcCol)
        WHERE row=new.DstRow and col=new.DstCol;




    UPDATE BOARD
        SET TILE_CONTENT = 'BLACK_TILE'
        WHERE row=new.SrcRow and col=new.SrcCol;
 /*   end;
end if;
*/


NOTIFY update_gui;
return null;
   
end;
$$language plpgsql;




Create or replace trigger on_move
before insert
on moves
for each row execute procedure make_move()



