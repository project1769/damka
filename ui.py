import pygame

SQUARE_WIDTH = 800

ROWS = 8

pygame.init()
WINDOW = pygame.display.set_mode((SQUARE_WIDTH, SQUARE_WIDTH))
pygame.display.set_caption("Checkers")

STRING_TO_IMAGE = {
    "RED_PIECE": pygame.transform.scale(pygame.image.load("resources/red.png"), (100, 100)).convert(),
    "BLACK_PIECE": pygame.transform.scale(pygame.image.load("resources/black.png"), (100, 100)).convert(),
    "RED_QUEEN": pygame.transform.scale(pygame.image.load("resources/redKing.png"), (100, 100)).convert(),
    "BLACK_QUEEN": pygame.transform.scale(pygame.image.load("resources/blackKing.png"), (100, 100)).convert()
    }

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
ORANGE = (235, 168, 52)
BLUE = (76, 252, 241)


class Node:
    def __init__(self, row, col, width):
        self.row = row
        self.col = col
        self.x = int(row * width)
        self.y = int(col * width)
        self.colour = WHITE
        self.piece = None

    def draw(self):
        if self.piece:
            WINDOW.blit(self.piece.image, (self.x, self.y))
        else:
            pygame.draw.rect(
                WINDOW,
                self.colour,
                (self.x, self.y, SQUARE_WIDTH / ROWS, SQUARE_WIDTH / ROWS),
            )
        


class Piece:
    def __init__(self, team):
        self.team = team
        self.image = STRING_TO_IMAGE[team]
        self.type = None

    def draw(self, x, y):
        WINDOW.blit(self.image, (x, y))


def get_current_tile():
    gap = SQUARE_WIDTH // ROWS
    RowX, RowY = pygame.mouse.get_pos()
    Row = RowX // gap
    Col = RowY // gap
    return (Col, Row)


def draw_empty_grid():
    gap = SQUARE_WIDTH // ROWS
    for i in range(ROWS):
        pygame.draw.line(WINDOW, BLACK, (0, i * gap), (SQUARE_WIDTH, i * gap))
        for j in range(ROWS):
            pygame.draw.line(WINDOW, BLACK, (j * gap, 0), (j * gap, SQUARE_WIDTH))
