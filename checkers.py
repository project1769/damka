import pygame
import logging
import select
import sys
import json

import ui
from checkers_db import CheckersDB
from board import Board
from moves import Moves


def main():
    logging.getLogger().setLevel(logging.DEBUG)

    with open("config.json", "r") as fp: 
        config = json.load(fp)

    checkers_db = CheckersDB(config["username"], config["password"])
    checkers_db.initialize_backend()
    board = Board(checkers_db)
    board.create()

    moves = Moves(checkers_db)
    moves.create()

    checkers_db.update_display()

    srcPosition = dstPosition = None

    checkers_db.init_logic(config["solution_file"])

    while True:
        checkers_db.print_notice_messages()

        if select.select([checkers_db.connection], [], [], 0) is not ([], [], []):
            checkers_db.connection.poll()
            while checkers_db.connection.notifies:
                notify = checkers_db.connection.notifies.pop(0)
                logging.debug(f"Got NOTIFY in channel {notify.channel}")
                checkers_db.update_display()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                logging.debug("EXIT SUCCESSFUL")
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                clickedNode = ui.get_current_tile()
                if srcPosition is None:
                    srcPosition = clickedNode
                else:
                    dstPosition = clickedNode
                    moves.update(srcPosition, dstPosition)
                    srcPosition = dstPosition = None


if __name__ == "__main__":
    main()
