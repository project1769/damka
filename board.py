from ui import ROWS

class Board:
    def __init__(self, db):
            self.db = db

    def create(self):
        grid = self._make_board(ROWS)
        with self.db.connection.cursor() as cursor: 
            cursor.execute("""DROP TABLE IF EXISTS BOARD""")
            cursor.execute(
            """CREATE TABLE BOARD
            (
                ROW INT,
                COL INT,
                TILE_CONTENT TEXT
            )"""
            )

            prototype = ',\n'.join(['''(%s, %s, %s)'''] * 64)
            entries = [[row, column, grid[row][column]] for row in range(8) for column in range(8)]

            cursor.execute(
                f"""INSERT INTO BOARD (ROW, COL, TILE_CONTENT)
            VALUES {prototype}""",
                tuple([entry for nested in entries for entry in nested]),
            )

    @staticmethod
    def _make_board(rows):
        board = []
        for i in range(rows):
            board.append([])
            for j in range(rows):
                current_cell = "WHITE_TILE"
                if abs(i - j) % 2 == 0:
                    current_cell = "BLACK_TILE"
                if (abs(i + j) % 2 == 0) and (i < 3):
                    current_cell = "RED_PIECE"
                elif (abs(i + j) % 2 == 0) and i > 4:
                    current_cell = "BLACK_PIECE"
                board[i].append(current_cell)
        return board
    