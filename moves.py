import logging

class Moves:
    def __init__(self, db):
        self.db = db

    def create(self):
        logging.debug(f"Creating MOVES table")
        with self.db.connection.cursor() as cursor:
            cursor.execute("""DROP TABLE IF EXISTS MOVES""")
            cursor.execute(
                """CREATE TABLE IF NOT EXISTS MOVES 
            (SrcRow int, SrcCol int, DstRow int, DstCol int)"""
            )

    def update(self, srcPosition, dstPosition):
        logging.debug(
            f"Updating move table with srcPosition = {srcPosition}, dstPosition = {dstPosition}"
        )
        with self.db.connection.cursor() as cursor:
            cursor.execute(
                f"""INSERT INTO MOVES 
            (SrcRow, SrcCol, DstRow, DstCol) 
            VALUES ({srcPosition[0]}, {srcPosition[1]}, {dstPosition[0]}, {dstPosition[1]})"""
            )