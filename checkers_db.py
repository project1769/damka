import psycopg2
import psycopg2.extensions
import pygame
import logging

import ui

class CheckersDB:
    def __init__(self, username, password):
        self.connection = None
        self.dbname = 'checkers'
        self.username = username
        self.password = password

    def initialize_backend(self):
        self._create_connection()
        self._create_db(self.dbname)
        self._recreate_connection(self.dbname)
        self._reset_schema()
        self._create_listener()

    def _create_connection(self, dbname='postgres'):
        try:
            self.connection = psycopg2.connect(f"user='{self.username}' password='{self.password}' host='localhost' dbname='{dbname}'")
            self.connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            self._last_notices_count = 0
        except psycopg2.OperationalError:
            raise RuntimeError('Connection to PSQL DB failed, make sure the data inside config.json is correct and that you are able to use PgAdmin with another DB.')

    def _create_db(self, dbname):
        try:
            with self.connection.cursor() as cursor:
                cursor.execute(f"CREATE DATABASE {dbname}")
        except psycopg2.errors.DuplicateDatabase:
            pass

    def _recreate_connection(self, dbname):
        self.connection.close()
        self._create_connection(dbname)

    def _reset_schema(self):
        with self.connection.cursor() as cursor:
            cursor.execute("DROP SCHEMA IF EXISTS public CASCADE;")
            cursor.execute("CREATE SCHEMA public;")

    def _create_listener(self):
        with self.connection.cursor() as cursor:
            cursor.execute("LISTEN update_gui;")

    def update_display(self):
        board = self.get_board()
        grid = self.board_to_grid(board)

        for row in grid:
            for spot in row:
                spot.draw()

        ui.draw_empty_grid()
        pygame.display.update()

    def board_to_grid(self, board):
        gap = ui.SQUARE_WIDTH // ui.ROWS
        
        grid = list([None] * ui.ROWS for _ in range(ui.ROWS))

        for entry in board:
            col, row, content = entry
            node = ui.Node(row, col, gap)
            if content == "WHITE_TILE":
                node.colour = ui.WHITE
            elif content == "BLACK_TILE":
                node.colour = ui.BLACK
            else:
                node.piece = ui.Piece(content)
            grid[row][col] = node
        return grid

    def get_board(self):
        cursor = self.connection.cursor()
        cursor.execute(
            f"""select * from BOARD order by row, col"""
        )
        new_board = cursor.fetchall()
        cursor.close()
        return new_board


    def init_logic(self, sql_name):
        cursor = self.connection.cursor()
        with open(sql_name, "r") as sql_setup:
            all_setup = sql_setup.read()
            if sql_setup.tell() == 0:
                logging.warning(f"{sql_name} file is empty")
                return
                
            try:
                cursor.execute(all_setup)

            except psycopg2.ProgrammingError:
                raise RuntimeError(f"{sql_name} code is malformed")


        cursor.close()
        logging.info(f"Finished loading {sql_name}")

    def print_notice_messages(self):
        if len(self.connection.notices) <= self._last_notices_count:
            return

        if self._last_notices_count == 0:
            self._last_notices_count = len(self.connection.notices)
            return

        for notice in self.connection.notices[self._last_notices_count:]:
            print(f'[NOTICE FROM SQL]: {notice}', end='')
        self._last_notices_count = len(self.connection.notices)